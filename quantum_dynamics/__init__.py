"""
This package contains various tools for time-dependent quantum mechanics.
"""

__author__ = "Janne Solanpää"
__copyright__ = "Copyright 2018, Janne Solanpää"
__author_email__ = "janne+compphys@solanpaa.fi"
__license__ = "Boost Software License 1.0"
__version__ = "0.1dev6"
