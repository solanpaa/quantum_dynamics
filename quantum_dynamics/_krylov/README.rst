Krylov subspace methods
-----------------------

This package contains an implementation for expm_multiply using Krylov-subspace techniques.
Our implementation is partly based on the MIT-licensed Krypy-package (https://github.com/andrenarchy/krypy), but mostly on the papers

R. B. Sidje, ACM Trans. Math. Softw., 24, 130-156 (1998)

and

Y. Saad, SIAM J. Numer. Anal., 29, 209–228 (1992).
