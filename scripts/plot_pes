#!/usr/bin/env python3

"""
A script for plotting the photoelectron spectrum from
a simulation made with qdyn_laser. Uses the window method
from Phys. Rev. A 42, 5794 (1990).
"""

import argparse
import numpy as np
import scipy.sparse as sp
from scipy.integrate import simps
import h5py
from quantum_dynamics.tise import get_initial_state
import progressbar
import numba

@numba.njit
def potential(x):
    """1D Hydrogen model potential"""
    return -1.0 / np.sqrt(x**2 + 1)

if __name__ == '__main__':

    parser = argparse.ArgumentParser(description="""Visualization of the
    photoelectron spectrum.""")

    parser.add_argument('--delta-energy', '-dE', type=float, default=5e-3,
                        help="Time-step")
    parser.add_argument('--max-energy', '-Emax', type=float, default=20,
                        help="Maximum simulated time")
    parser.add_argument('--file', '-f', type=str, default="qdyn.h5",
                        help="Path where to load the simulation data")

    parser.add_argument("--nogui", action="store_true",default=False)
    parser.add_argument("-o", "--out", type=str, default="")


    args = parser.parse_args()

    # Setup matplotlib if no gui is asked
    if args.nogui:
        import matplotlib
        matplotlib.use('Agg')
        assert args.out != "", "You forgot the give me the output filename"

    import matplotlib.pyplot as plt

    # Parse arguments
    delta_energy = args.delta_energy
    max_energy = args.max_energy
    filename = args.file
    
    # Open savefile, fail if it doesn't exist or cannot read data from it
    try:
        with h5py.File(filename, 'r') as f:
                final_wf = f['final_wavefunction'][:]
                grid = f['coordinate_grid'][:]
                try:
                    PES = f['PES'][:]
                except Exception as e:
                    print(e)
                    PES = None
    except:
        print("Could not load data from the savefile.")
        exit(1)

    # If savefile didn't include precomputed PES, let's compute it
    if PES is None:
        # Get the initial state and time-independent part of the Hamiltonian
        _, H0 = get_initial_state(grid, potential, 'csr')

        # Set the energies, and preallocate PES array
        gamma = delta_energy/2.0
        gamma2j = 1j*gamma**2
        gamma4 = gamma**4

        energies = np.arange(gamma, max_energy, delta_energy)

        PES = np.zeros((len(energies), 2))
        PES[:,0] = energies
        ONE = sp.eye(H0.shape[0], format='csr')
        
        # For every energy, compute the PES
        for i, E in progressbar.progressbar(enumerate(energies),  max_value=len(energies)):
            pes_tmp_chi = sp.linalg.spsolve( (H0-E*ONE)**2 + gamma2j*ONE, final_wf) 
            PES[i,1] = simps(np.abs(pes_tmp_chi)**2, grid)
        
        # Finally, save PES to the file
        with h5py.File( filename, 'a') as f:
            f['PES'] = PES


    # Plot PES

    w,h = plt.figaspect( (np.sqrt(5)-1)/2 )

    fig = plt.figure( figsize = (w,h) )
 
    ax = fig.add_subplot(111)
    ax.plot(PES[:,0], PES[:,1])
    ax.set_yscale('log')
    ax.set_xlabel('energy (a.u.)')
    ax.set_ylabel('electron yield')
    fig.tight_layout()

    if args.out != "":
        plt.savefig(args.out)

    if not args.nogui:
        plt.show()


